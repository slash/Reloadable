# Reloadable

* 利用zookeeper的监听事件，动态改变property的value。
* 重载了spring的PropertyPlaceholder，加载新的PropertySource，从zk取数据。
在xml中定义的${placeholder}不能动态重新加载
* 无缝衔接spring的@Value注解，可通过修改zk中对应的值，动态修改应用中的属性。

## DEMO

[demo](https://git.oschina.net/slash/reloadable-demo)

## 参考文章

* [http://colobu.com/tags/Curator/](http://colobu.com/tags/Curator/)
* [http://jm.taobao.org/2016/09/28/an-article-about-config-center/](http://jm.taobao.org/2016/09/28/an-article-about-config-center/)
* [http://www.iteye.com/topic/1122859](http://www.iteye.com/topic/1122859)
* [https://git.oschina.net/xuxueli0323/xxl-conf](https://git.oschina.net/xuxueli0323/xxl-conf)

## 后续改进

缺少一个配置管理中心