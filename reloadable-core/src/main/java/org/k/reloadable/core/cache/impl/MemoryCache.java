package org.k.reloadable.core.cache.impl;

import org.k.reloadable.core.cache.ICache;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by hengxianwang on 16-12-19.
 */
public class MemoryCache implements ICache{
    private Map<String,Object> cache = new HashMap<String, Object>();

    @Override
    public <T> void set(String key, T value) {
        cache.put(key, value);
    }

    @Override
    public <T> T get(String key) {
        return (T)cache.get(key);
    }

    @Override
    public void del(String key) {
        this.cache.remove(key);
    }
}
