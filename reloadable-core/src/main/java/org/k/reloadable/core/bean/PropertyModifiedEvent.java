package org.k.reloadable.core.bean;

/**
 * Created by hengxianwang on 16-12-20.
 */
public class PropertyModifiedEvent {
    private String propertyName;
    private Object value;
    private Event event;

    public PropertyModifiedEvent() {
    }

    public PropertyModifiedEvent(String propertyName, Object value, Event event) {
        this.propertyName = propertyName;
        this.value = value;
        this.event = event;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }
}
