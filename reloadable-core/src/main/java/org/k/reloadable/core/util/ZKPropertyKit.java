package org.k.reloadable.core.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by hengxianwang on 16-12-21.
 */
public class ZKPropertyKit {
    private static String ZKSERVER_PROPERTY = "zkservers.properties";
    private static String ZKSERVERS = "zkservers";
    static Properties properties = null;
    static {
        InputStream in = ZKPropertyKit.class.getClassLoader().getResourceAsStream(ZKSERVER_PROPERTY);
        try {
            properties = new Properties();
            properties.load(in);
        } catch (IOException e) {

        }finally {
            if(in != null) try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static String getZKServers(){
        return properties.getProperty(ZKSERVERS);
    }

}
