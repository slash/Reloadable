package org.k.reloadable.core.cache;

/**
 * Created by hengxianwang on 16-12-19.
 */
public interface ICache {

    public <T> void set(String key, T value);

    public <T> T get(String key);

    public void del(String key);
}
