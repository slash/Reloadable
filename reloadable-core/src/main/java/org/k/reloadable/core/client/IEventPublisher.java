package org.k.reloadable.core.client;

import org.k.reloadable.core.bean.PropertyModifiedEvent;

/**
 * Created by hengxianwang on 16-12-19.
 */
public interface IEventPublisher {
    public void publish(PropertyModifiedEvent propertyModifiedEvent);
}
