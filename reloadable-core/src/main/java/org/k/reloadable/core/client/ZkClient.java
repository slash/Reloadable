package org.k.reloadable.core.client;

import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.k.reloadable.core.util.ZKPropertyKit;

/**
 * Created by hengxianwang on 16-12-21.
 */
public class ZkClient {
    public static String ROOT_NODE_NAME = "/reloadable";
    static CuratorFramework client;
    static String connectString = ZKPropertyKit.getZKServers();
    static {
        RetryPolicy retryPolicy = new ExponentialBackoffRetry(1000, 3);
        client = CuratorFrameworkFactory.newClient(connectString, retryPolicy);
        client.start();
    }

    public static String getNodeData(String nodeName) throws Exception {
        return new String(client.getData().forPath(ROOT_NODE_NAME + "/" + nodeName));
    }

}
