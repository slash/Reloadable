package org.k.reloadable.core;

import org.k.reloadable.core.bean.PropertyModifiedEvent;

import java.util.*;

/**
 * Created by hengxianwang on 16-12-19.
 */
public class Reloadable {

    private List<ReloadObserver> observers = new ArrayList<ReloadObserver>();

    public void change(PropertyModifiedEvent propertyModifiedEvent){
        this.notifyObservers(propertyModifiedEvent);
    }

    protected void notifyObservers(PropertyModifiedEvent holder){
        for(ReloadObserver observer:observers){
            observer.update(holder);
        }
    }

    public void addObserver(ReloadObserver observer){
       this.observers.add(observer);
    }


}
