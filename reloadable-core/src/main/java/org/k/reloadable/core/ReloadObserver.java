package org.k.reloadable.core;

import org.k.reloadable.core.bean.PropertyModifiedEvent;

/**
 * Created by hengxianwang on 16-12-19.
 */
public interface ReloadObserver {

    public void update(PropertyModifiedEvent propertyModifiedEvent);
}
