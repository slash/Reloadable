package org.k.reloadable.spring;

import org.k.reloadable.core.client.ConfClient;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.*;
import org.springframework.util.Assert;

import java.io.IOException;

/**
 * Created by hengxianwang on 16-12-20.
 */
public class ReadablePropertySourcesPlaceholderConfigurer extends PropertySourcesPlaceholderConfigurer{

    private MutablePropertySources propertySources;
    private Environment environment;
    private PropertySources appliedPropertySources;

    private ConfClient confClient;

    public static final String REMOTE_PROPERTIES_PROPERTY_SOURCE_NAME = "remoteProperties";

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        if (this.propertySources == null) {
            this.propertySources = new MutablePropertySources();
            if (this.environment != null) {
                this.propertySources.addLast(
                        new PropertySource<Environment>(ENVIRONMENT_PROPERTIES_PROPERTY_SOURCE_NAME, this.environment) {
                            @Override
                            public String getProperty(String key) {
                                return this.source.getProperty(key);
                            }
                        }
                );
            }

            //加入zk中的数据
            this.propertySources.addLast(new PropertySource<ConfClient>(REMOTE_PROPERTIES_PROPERTY_SOURCE_NAME, this.confClient) {
                @Override
                public Object getProperty(String key) {
                    return this.source.get(key);
                }
            });

            try {
                PropertySource<?> localPropertySource =
                        new PropertiesPropertySource(LOCAL_PROPERTIES_PROPERTY_SOURCE_NAME, mergeProperties());
                if (this.localOverride) {
                    this.propertySources.addFirst(localPropertySource);
                }
                else {
                    this.propertySources.addLast(localPropertySource);
                }
            } catch (IOException ex) {
                throw new BeanInitializationException("Could not load properties", ex);
            }


        }

        processProperties(beanFactory, new PropertySourcesPropertyResolver(this.propertySources));
        //this.appliedPropertySources = this.propertySources;
    }
    @Override
    public void setIgnoreUnresolvablePlaceholders(boolean ignoreUnresolvablePlaceholders) {
        super.setIgnoreUnresolvablePlaceholders(true);
    }

    public ConfClient getConfClient() {
        return confClient;
    }

    public void setConfClient(ConfClient confClient) {
        this.confClient = confClient;
    }

    @Override
    public PropertySources getAppliedPropertySources() throws IllegalStateException {
        return this.propertySources;
    }
}
