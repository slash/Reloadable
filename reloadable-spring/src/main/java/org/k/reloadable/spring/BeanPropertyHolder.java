package org.k.reloadable.spring;

import java.lang.reflect.Field;

/**
 * Created by hengxianwang on 16-12-19.
 */
public class BeanPropertyHolder {
    private final Object bean;
    private final Field field;
    public BeanPropertyHolder(Object bean, Field field){
        this.bean = bean;
        this.field = field;
    }

    public Object getBean() {
        return bean;
    }

    public Field getField() {
        return field;
    }
}
